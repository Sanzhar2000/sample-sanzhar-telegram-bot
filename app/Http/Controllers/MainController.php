<?php

namespace App\Http\Controllers;

use App\Category;
use App\Telegram\Commands\StartCommand;
use Illuminate\Http\Request;
use Telegram\Bot\Api;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\TelegramResponse;

class MainController extends Controller
{
    protected $telegram;

    public function __construct()
    {
        $this->telegram = new Api(config('telegram.bot_token'));
    }

    public function set_Webhook()
    {
        $update = $this->telegram->commandsHandler(true);

        return $this->telegram->setWebhook([
            'url' => 'https://sample-sanzhar-telegram-bot.herokuapp.com/' . config('telegram.bot_token') . '/webhook'
        ]);
    }

    public function remove_Webhook()
    {
        return $this->telegram->removeWebhook();
    }

    public function getCategoriesKeyboardReplyMarkup()
    {
        $array = Category::pluck('name')->toArray();
        $keyboard = [];
        $row = [];
        $count = 0;

        foreach ($array as $key => $value){
            if ($count == 2){
                array_push($keyboard, $row);
                $row = [];
                $count = 0;
            }
            if ($key == array_key_last($array)) {
                array_push($row, $value);
                array_push($keyboard, $row);
                $row = [];
                $count = 0;
                // last element
            } else {
                array_push($row, $value);
                $count++;
                // not last element
            }
        }

        return $this->telegram->replyKeyboardMarkup([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);
    }

    public function getLanguagesKeyboardReplyMarkup()
    {
        $keyboard = [
            ['EN'],
            ['RU'],
            ['KK'],
        ];

        return $this->telegram->replyKeyboardMarkup([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);
    }

    public function index()
    {
        $updates = $this->telegram->getWebhookUpdates();

        $message = $updates->getMessage();
        $user = $message->getFrom();
        $text = $message->getText();

        if ($text == 'EN' || $text == 'RU' || $text == 'KK'){
            $this->telegram->sendMessage([
                'chat_id' => $user->getId(),
                'text' => 'Sample text Languages: ' . $text,
                //'reply_markup' => $reply_markup,
            ]);
        }else{
            $reply_markup = $this->getLanguagesKeyboardReplyMarkup();

            $this->telegram->sendMessage([
                'chat_id' => $user->getId(),
                'text' => 'Sample text Languages',
                'reply_markup' => $reply_markup,
            ]);
        }
    }
}
