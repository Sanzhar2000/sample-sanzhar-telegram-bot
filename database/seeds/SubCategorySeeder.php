<?php

use App\SubCategory;
use Illuminate\Database\Seeder;

class SubCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 4; $i++){
            for ($j = 1; $j <= 4; $j++){
                SubCategory::create([
                    'category_id' => $i,
                    'name' => "SubCategory ". $i . " : " . $j
                ]);
            }
        }
    }
}
