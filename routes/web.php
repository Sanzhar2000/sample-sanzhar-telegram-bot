<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\MainController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::post('/' . config('telegram.bot_token') . '/webhook', [MainController::class, 'index']);

Route::get('/test', [MainController::class, 'test']);

Route::get('set/webhook', [MainController::class, 'set_Webhook']);
Route::get('remove/webhook', [MainController::class, 'remove_Webhook']);

//Route::get('/set/webhook', function () {
//    $api = new \Telegram\Bot\Api(config('telegram.bot_token'));
//
//});
//Route::get('set/webhook', function () {
//    $response = \Telegram\Bot\Laravel\Facades\Telegram::setWe
//});
